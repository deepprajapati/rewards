from django.utils.translation import gettext_lazy as _
from rest_framework import serializers
from .utils import is_leap


class LeapYearSerializer(serializers.Serializer):
    start = serializers.IntegerField(write_only=True, min_value=1)
    end = serializers.IntegerField(write_only=True, min_value=1)
    leap_years = serializers.ListField(read_only=True)

    class Meta:
        fields = ("start", "end", "leap_years")

    def validate(self, attrs):
        if attrs["start"] >= attrs["end"]:
            raise serializers.ValidationError(
                {"non_field_errors": [_("Start Year must be less than End Year")]}
            )

        return super(LeapYearSerializer, self).validate(attrs)

    def create(self, validated_data):
        start = validated_data.get("start")
        end = validated_data.get("end")

        # get list of leap year from the start Year and end Year
        leap_years = list(filter(lambda year: is_leap(year), range(start, end)))
        return {"leap_years": leap_years}
