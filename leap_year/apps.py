from django.apps import AppConfig


class LeapYearConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'leap_year'
