from django.utils.translation import gettext_lazy as _
from rest_framework import status
from rest_framework.generics import GenericAPIView, CreateAPIView
from rest_framework.response import Response

from .serializers import LeapYearSerializer
from .utils import is_leap


class LeapYearView(CreateAPIView, GenericAPIView):
    """
        #Leap Year endpoint#

        **POST: /api/v1/leap-year/**

            {
                "start": 2000,
                "end": 2022,
            }

        **Response**

            {
                "leap_years": [
                    2000,
                    2004,
                    2008,
                    2012,
                    2016,
                    2020
                ]
            }

        **GET: /api/v1/leap-year/?year=2020**

            {
                "message": "2020 is a Leap Year"
            }

        ## Fields Legend: ##

            * start - integer
            * start - integer
            * leap_years - Array of Integer
            * message - string
        """
    serializer_class = LeapYearSerializer

    def get(self, request, *args, **kwargs):
        year = request.GET.get("year")
        if not (year and year.isdigit()):
            return Response(
                {"non_field_errors": [_("Year is required and must be integer")]},
                status=status.HTTP_400_BAD_REQUEST,
            )

        if is_leap(int(year)):
            return Response(
                {"message": _(f"{year} is a Leap Year")}, status=status.HTTP_200_OK
            )
        return Response(
            {"message": _(f"{year} is not a Leap Year")}, status=status.HTTP_200_OK
        )
