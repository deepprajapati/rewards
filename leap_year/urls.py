from django.urls import path
from .views import LeapYearView

urlpatterns = [
    path('leap-year/', LeapYearView.as_view(), name='leap-year'),
]
